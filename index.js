/* eslint-disable import/no-dynamic-require */
const _ = require('lodash');
const connect = require('connect');
const path = require('path');
const defaultConfig = require('./config.json');

const isntRouter = name => name !== 'router';

function catena(cfg, logger) {
  const config = cfg || {};
  const order = _.defaultTo(config.order, defaultConfig.order);
  const noRouter = _.every(order, isntRouter);
  const beforeRouter = _.takeWhile(order, isntRouter);
  const afterRouter = _.takeRightWhile(order, isntRouter);

  const before = connect();
  beforeRouter.forEach((name) => {
    if (defaultConfig.middlewares[name] === undefined) {
      throw new Error(`Unrecognised middleware identifier: '${name}'`);
    }
    const defaults = defaultConfig.defaults[name];
    const providedConfig = _.cloneDeep(config[name]);
    const middlewareConfig = _.defaults(providedConfig, defaults);
    if (providedConfig === false
      || (_.isUndefined(providedConfig) && defaults === false)) {
      return;
    }
    const middleware = catena[name](middlewareConfig, logger);
    if (middleware) {
      before.use(middleware);
    }
  });

  if (noRouter) {
    return [before];
  }

  const after = [];
  afterRouter.forEach((name) => {
    if (defaultConfig.middlewares[name] === undefined) {
      throw new Error(`Unrecognised middleware identifier: '${name}'`);
    }
    const defaults = defaultConfig.defaults[name];
    const providedConfig = _.cloneDeep(config[name]);
    const middlewareConfig = _.defaults(providedConfig, defaults);
    if (providedConfig === false
      || (_.isUndefined(providedConfig) && defaults === false)) {
      return;
    }
    const middleware = catena[name](middlewareConfig, logger);
    if (middleware) {
      after.push(middleware);
    }
  });

  return [before, after];
}

Object.keys(defaultConfig.middlewares).forEach((moduleName) => {
  const pkg = require(path.resolve(__dirname, `./lib/${moduleName}`));
  const aliases = defaultConfig.middlewares[moduleName];
  catena[moduleName] = pkg;
  aliases.forEach((aliasName) => {
    catena[aliasName] = pkg;
  });
});

module.exports = catena;
