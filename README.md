# Catena

THIS REPO IS DEAD, YOU SHOULD USE THE [VERSION IN SPARTA-INFRA](https://bitbucket.org/elliptic/sparta-infra/src/master/libs/catena/) INSTEAD

---

A collection of middleware (with sensible defaults) for use in an express app. Inspired by, and incorporating helmet.

Quick start
-----

Run `npm install catena --save` for your app. Then:

```js
const express = require('express');
const catena = require('catena');

const app = express();
const [before, after] = catena();

// important to enable this if you are behind a proxy and have enforcesSSL
// enabled as a middleware (which is the case in default setup)
app.enable('trust proxy');
app.use(before);

// add routing
app.use('*', (req, res, next) => res.json({ message: 'Hello World'}));

app.use(after);
```

[TODO]
