module.exports = (config) => {
  const msg = `The API is currently offline for maintenance. \
    Please try again in a few minutes. \
    If the problem persists please contact ${config.support}`;

  return (req, res, next) => {
    // if not maintenance mode, or a preflight request, then continue as normal
    if ((!config.enabled) || (req.method === 'OPTIONS')) {
      return next();
    }
    return res.status(503).json(msg);
  };
};
