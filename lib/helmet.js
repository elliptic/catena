const connect = require('connect');
const helmet = require('helmet');

module.exports = (config) => {
  const chain = connect();
  if (config.csp) {
    chain.use(helmet.contentSecurityPolicy(config.csp));
  }
  chain.use(helmet.hsts(config.hsts));
  chain.use(helmet.xssFilter(config.xssFilter));
  chain.use(helmet.frameguard(config.frameguard));
  chain.use(helmet.hidePoweredBy());
  chain.use(helmet.ieNoOpen());
  chain.use(helmet.noSniff());
  chain.use(helmet.noCache());
  return chain;
};
