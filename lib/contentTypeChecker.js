module.exports = function factory(config) {
  return function contentTypeChecker(req, res, next) {
    if (req.method === 'OPTIONS' || config.types.includes(req.get('Content-Type'))) {
      return next();
    }

    return next({ status: 415 });
  };
};
