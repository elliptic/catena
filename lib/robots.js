module.exports = () =>
  function robots(req, res, next) {
    if (req.url === '/robots.txt') {
      res.type('text/plain');
      return res.send('User-agent: *\nDisallow: /');
    }
    return next();
  };

