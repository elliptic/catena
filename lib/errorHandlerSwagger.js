const _ = require('lodash');

module.exports = (config, log) =>
  function swaggerErrorHandler(err, req, res, next) {
    if (err.failedValidation) {
      if (log && err.message.startsWith('Response') && !_.isUndefined(err.results)) {
        _.each(err.results.errors, log.error);
      }
      if (err.message.startsWith('Request')) {
        const payload = {
          name: 'BadRequestError',
          message: err.message,
        };
        if (err.code === 'SCHEMA_VALIDATION_FAILED') {
          if (!_.isUndefined(err.results)) {
            const errs = err.results.errors;
            if (!_.isUndefined(errs) && !(errs.length === 0)) {
              payload.errors = errs;
            }
          }
        }
        if (log) log.error(payload);
        return res.status(400).json(payload);
      }
    }
    return next(err);
  };

