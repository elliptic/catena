const winston = require('winston');
const expressWinston = require('express-winston');

expressWinston.requestWhitelist.push('id');
expressWinston.requestWhitelist.push('body');
expressWinston.requestWhitelist.push('ip');
expressWinston.requestWhitelist.push('actor');
expressWinston.requestWhitelist.push('authInfo');
expressWinston.requestWhitelist.push('routeParams');
expressWinston.requestWhitelist.push('routePath');
expressWinston.responseWhitelist.push('message');

module.exports = (config, log) =>
  expressWinston.errorLogger({
    winstonInstance: log || loggerFactory(),
  });
function loggerFactory() {
  return new winston.Logger({
    level: 'warn',
    transports: [new winston.transports.Console({ expressFormat: true })],
  });
}
