const favicon = require('serve-favicon');
const p = require('path');

module.exports = (config) => {
  let { path } = config;
  if (!path) {
    path = p.join(__dirname, '..', 'static/favicon.ico');
  }
  return favicon(path, config);
};
