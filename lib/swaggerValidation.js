const _ = require('lodash');
const connect = require('connect');
const swaggerMetadata = require('ell-fork-swagger-tools/middleware/swagger-metadata');
const swaggerValidator = require('ell-fork-swagger-tools/middleware/swagger-validator');

module.exports = (config) => {
  const { swaggerSpec, validator } = config;
  let swaggerDocs = swaggerSpec;
  if (!_.isArray(swaggerSpec)) {
    swaggerDocs = [swaggerSpec];
  }
  const chain = connect();
  swaggerDocs.forEach(doc => chain.use(swaggerMetadata(doc)));
  chain.use(swaggerValidator(validator));
  return chain;
};
