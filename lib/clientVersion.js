const SV = require('semver');
const _ = require('lodash');

const makeFailureCheck = ({ minimum, header }) => {
  const lessThan = current => SV.lt(current, minimum);
  const getter = req => req.get(header);
  return (req) => {
    const value = getter(req);
    if (value === undefined) return false;
    if (!SV.valid(value)) return true;
    return lessThan(value);
  };
};

module.exports = function factory(config) {
  const { msg, versions } = config;
  const checkers = versions.map(makeFailureCheck);
  return function clientVersion(req, res, next) {
    const anyFailure = _.some(checkers, fn => fn(req));
    if (anyFailure) {
      return res.status(490).json(msg);
    }
    return next();
  };
};
