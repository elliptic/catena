const shouldHandle = ({ status = 500 }) => (status > 399 && status < 500);

module.exports = () =>
  (err, req, res, next) => {
    if (!shouldHandle(err)) return next(err);

    const { name, message, status } = err;
    return res.status(status).json({ name, message, status });
  };
