const uuid = require('uuid/v4');
const validate = require('uuid-validate');

module.exports = () =>
  function requestId(req, res, next) {
    // use proxy/load balancer's generated ones if available, otherwise create our own.
    let id = req.headers['x-request-id'];
    if (!validate(id, 4)) {
      id = uuid();
    }
    req.id = id;
    return next();
  };
