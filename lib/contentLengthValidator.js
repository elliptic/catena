const contentLength = require('express-content-length-validator');

module.exports = (config) => {
  if (config.max <= 0) { throw new Error('invalid contentLength max config'); }
  const opts = {
    max: config.max,
    status: config.status != null ? config.status : 400,
    message: config.message != null ? config.message : `Content Length exceeded allowed maximum of ${config.max}`,
  };

  return contentLength.validateMax(opts);
};
