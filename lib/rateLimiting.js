const Limiter = require('express-rate-limit');

module.exports = config => new Limiter(config);
