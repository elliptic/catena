const middleware = require('ell-fork-swagger-tools/middleware/swagger-ui');

module.exports = (config) => {
  const {
    swaggerSpec,
    apiDocs = '/swagger/api_docs',
    swaggerUi = '/swagger/docs',
    swaggerUiDir,
  } = config;

  return middleware(swaggerSpec, { apiDocs, swaggerUi, swaggerUiDir });
};
