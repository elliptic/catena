const compression = require('compression');

module.exports = config => compression(config);
