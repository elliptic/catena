const cors = require('cors');

module.exports = config => cors(config);
