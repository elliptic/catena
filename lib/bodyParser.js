const bodyParser = require('body-parser');

module.exports = config => bodyParser.json(config);
