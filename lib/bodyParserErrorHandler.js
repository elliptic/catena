module.exports = config =>
  function bodyParserErrorHandler(err, req, res, next) {
    if (err != null) {
      if ([413, 415].includes(err.status)) {
        return res.sendStatus(err.status);
      } else if ((err instanceof SyntaxError) || (err.message === 'invalid json')) {
        return res.status(400).json({
          name: config.errorName,
          message: 'invalid json',
        });
      }
    }
    return next(err);
  };
