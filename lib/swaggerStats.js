const sws = require('swagger-stats');

module.exports = config => sws.getMiddleware(config);
