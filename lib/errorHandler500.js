module.exports = function factory(config, log) {
  const statusCode = 500;
  const errObj = {
    name: config.errorName,
    status: statusCode,
    message: 'Internal Server Error',
  };

  return function errorHandler500(err, req, res, next) {
    if (log) {
      const {
        code, description, message, name, stack, status, original_status,
      } = err;
      log.error({
        code, description, message, name, original_status, stack, status,
      });
    }
    return res.status(statusCode).json(errObj);
  };
};
